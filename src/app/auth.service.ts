import { Injectable, OnInit } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { AngularFireAuth } from '@angular/fire/compat/auth';
import { RecaptchaVerifier, getAuth } from '@firebase/auth';
import { Auth } from '@angular/fire/auth';
import * as firebase from 'firebase/app';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  public validating:boolean = false;
  public authError:string = "";
  public credentials:any = {
    phone: "+56",
    name: "",
    password: "",
    signed: false,
    admin: false,
  };
  public recaptchaVerifier:any;

  validateIfComplete(){
    if(this.credentials.admin && this.credentials.password ==  'lomiriders161'){
      this.credentials.signed = true;
      this.router.navigateByUrl("/orders")
      return
    }

    let phone = this.credentials.phone;
    phone = phone.replace("+","")
    if(phone.substring(0,2) == "56"){
      this.credentials.phone = "+"+phone
    } else{
      this.credentials.phone = "+56"+phone
    }
    if(this.credentials.phone.length == "12"){
      this.validatePhone()
    }
  }

  validateIfCompletePass(){
    let pass = this.credentials.password;
    if(pass.length == 6){
      this.validating = true;
      this.login();
    }
  }

  validatePhone = ()=>{
    this.validating = true;
    this.http.post(environment.hermexApiUrl+"/validatePhone",this.credentials).subscribe((res:any)=>{
      if(res.name){
        this.credentials.name = res.name
        this.validating = false;
        this.authError = ""
      } else if(res.error){
        this.validating = false;
        this.authError = "El codigo ingresado no es correcto"
      }
    })
  }

  async login(){
    this.authError = ""
    this.http.post(environment.hermexApiUrl+"/login",this.credentials).subscribe((res:any)=>{
      if(res.error){
        this.authError = res.error;
      } else {
        this.credentials.signed = true;
        this.router.navigateByUrl("/orders")
      }
      this.validating = false;
    })
  }

  logout(){
    this.credentials = null;
  }

  constructor(private http:HttpClient, private router:Router){
  }
  
  ngOnInit(){
  }

}
