import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {MatToolbarModule} from '@angular/material/toolbar';
import {MatSidenavModule} from '@angular/material/sidenav';
import { MatIconModule } from '@angular/material/icon';
import { MatListModule } from '@angular/material/list';
import { DarkstoresComponent } from './darkstores/darkstores.component';
import { OrdersComponent } from './orders/orders.component';
import { MatTableModule } from '@angular/material/table';
import { StoreComponent } from './darkstores/store/store.component';
import { OrderComponent } from './orders/order/order.component';
import { CourierComponent } from './courier/courier.component';
import {MatChipsModule} from '@angular/material/chips';
import {MatDialogModule} from '@angular/material/dialog';
import { RiderComponent } from './rider/rider.component';
import { RidersComponent } from './riders/riders.component';
import { MapComponent } from './map/map.component';
import { AgmCoreModule } from '@agm/core';
import { HttpClientModule } from '@angular/common/http';
import { AgmDirectionModule } from 'agm-direction';
import { MenuComponent } from './map/menu/menu.component';   // agm-direction
import {MatSlideToggleModule} from '@angular/material/slide-toggle';
import { FormsModule } from '@angular/forms';
import { DashboardComponent } from './dashboard/dashboard.component';
import {MatCardModule} from '@angular/material/card';
import { SocketIoModule, SocketIoConfig } from 'ngx-socket-io';
import { GoogleChartsModule } from 'angular-google-charts';
import {MatExpansionModule} from '@angular/material/expansion';
import { MatPaginatorModule } from '@angular/material/paginator';
import { LoginComponent } from './login/login.component';
import {MatInputModule} from '@angular/material/input';
import { initializeApp,provideFirebaseApp } from '@angular/fire/app';
import { environment } from '../environments/environment';
import { provideAuth,getAuth } from '@angular/fire/auth';
import { getFirestore, provideFirestore } from '@angular/fire/firestore';
import { PagesComponent } from './pages/pages.component';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';
import {MatButtonModule} from '@angular/material/button';
<<<<<<< HEAD
import {MatSnackBarModule} from '@angular/material/snack-bar';
=======
import {MatBottomSheetModule} from '@angular/material/bottom-sheet';
>>>>>>> old-console

const config: SocketIoConfig = { url: environment.socketUrl, options: {} };

@NgModule({
  declarations: [
    AppComponent,
    DarkstoresComponent,
    OrdersComponent,
    StoreComponent,
    OrderComponent,
    CourierComponent,
    RiderComponent,
    RidersComponent,
    MapComponent,
    MenuComponent,
    DashboardComponent,
    LoginComponent,
    PagesComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatToolbarModule,
    MatSidenavModule,
    MatIconModule,
    MatListModule,
    MatTableModule,
    MatChipsModule,
    HttpClientModule,
    MatDialogModule,
    MatBottomSheetModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyDY6JRoW_FGIBo4tld_Y7jEL-83NWiX-lw'
    }),
    AgmDirectionModule,
    MatSlideToggleModule,
    FormsModule,
    MatCardModule,
    SocketIoModule.forRoot(config),
    GoogleChartsModule,
    MatExpansionModule,
    MatPaginatorModule,
    MatInputModule,
    MatProgressSpinnerModule,
    MatButtonModule,
    MatSnackBarModule
  ],
  providers: [
    
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
