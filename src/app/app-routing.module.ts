import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DarkstoresComponent } from './darkstores/darkstores.component';
import { StoreComponent } from './darkstores/store/store.component';
import { OrdersComponent } from './orders/orders.component';
import { MapComponent } from './map/map.component';
import { RidersComponent } from './riders/riders.component';
import { DashboardComponent  } from './dashboard/dashboard.component';
import { RiderComponent } from './rider/rider.component';
import { OrderComponent } from './orders/order/order.component';
import { LoginComponent } from './login/login.component';
import { PagesComponent } from './pages/pages.component';

const routes: Routes = [
  { path: "login" , component: LoginComponent},
  { path: "pages", component: PagesComponent},
  { path: "map", component: MapComponent},
  { path: "darkstores/:store", component: StoreComponent },
  { path: "darkstores" , component: DarkstoresComponent},
  { path: "orders", component: OrdersComponent},
  { path: "orders/:orderId", component: OrderComponent},
  { path: "riders", component: RidersComponent},
  { path: "riders/:rider", component: RiderComponent},
  { path: "", component:OrdersComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
