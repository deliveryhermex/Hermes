import { Component, Input, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { RiderComponent } from 'src/app/rider/rider.component';
import { RidersService } from 'src/app/services/riders.service';
import { TrackingService } from '../services/tracking.service';
import { OrdersService  } from '../services/orders.service';
@Component({
  selector: 'app-riders',
  templateUrl: './riders.component.html',
  styleUrls: ['./riders.component.scss']
})
export class RidersComponent implements OnInit {

  @Input() trackingServiceStatus:any;

  public events = [
    [0,"Disponible"],
    [2,"Pedido asignado"],
    [3,"Pedido aceptado"],
    [4,"Cercano a tienda"],
    [5 ,"En tienda"],
    [6,"Cercano a destino"],
    [7,"En destino"],
    [8, "Pedido entregado"]

  ]

  public store = { 
    name: "Las Condes",
    id: 1,
    queued: 32,
    dispatched: 1,
    active: 25,
    delay: 6,
    temp: 0,
    break: 0,
    work: 42,
    start: 18
  }

  getState(riderId:any){
    let order = this.ordersService.orders
    .filter((order)=>order.currentStatusId? order.currentStatusId != 8 : false)
    .find((order)=>riderId == order.riderId)
    return ( order ? order.currentStatusId: null)
  } 

  public getInState(state: any, vehicle: string){
    return this.trackingService.tracked.filter((rider:any) => this.getState(rider.id) == state);
  }

  constructor(public ordersService:OrdersService, public trackingService:TrackingService, public dialog: MatDialog, public ridersService:RidersService) {}

  openDialog() {
    const dialogRef = this.dialog.open(RiderComponent);

    dialogRef.afterClosed().subscribe(result => {
      console.log(`Dialog result: ${result}`);
    });
  }



  ngOnInit(): void {
  }

}
