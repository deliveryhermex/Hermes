import { Component, OnInit } from '@angular/core';
import { MapOptions } from '../map.interfaces';
import { MapService } from '../map.service';
@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss']
})
export class MenuComponent implements OnInit {

  public mapOptions:any = {
    isochronus : {
      hidden: false
    },

    stores : {
      hidden: false
    },

    places : {
      hidden: false
    }
  }

  constructor(public mapService:MapService) {
  }

  public toggleIsochronus(){
    console.log(this.mapOptions.isochronus.hidden)
    this.mapService.editOption({isochronus:{
      hidden: this.mapOptions.isochronus.hidden
    }})
  }

  public togglePlaces(){
    this.mapService.editOption({places:{
      hidden: !this.mapOptions.places.hidden
    }})
  }

  public toggleStores(){
    this.mapService.editOption({stores:{
      hidden: !this.mapOptions.stores.hidden
    }})
  }

  ngOnInit(): void {
    this.mapService.mapOptions$.subscribe((mapOptions)=>{
      this.mapOptions = mapOptions
      console.log(this.mapOptions, "changed")
    })
  }

}
