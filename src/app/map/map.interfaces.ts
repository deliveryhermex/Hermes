export interface MapOptions{
    isochronus?: IsochronusOptions
    stores? : any
    places? : any
    riders?: any
}

export interface IsochronusOptions{
    hidden?: boolean,
}