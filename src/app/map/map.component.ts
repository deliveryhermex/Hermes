import { Component, Input, OnInit } from '@angular/core';
import { TrackingService } from 'src/app/services/tracking.service';
import { OrdersService } from 'src/app/services/orders.service';
import { MapService } from './map.service';
import { MapOptions } from './map.interfaces';
import { RidersService } from 'src/app/services/riders.service';

@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.scss']
})
export class MapComponent implements OnInit {

  @Input() zoom = 12;
  @Input() type = "default";
  @Input() polyline = []

  @Input() storePlace = {
    lng: 0,
    lat: 0
  }
  @Input() customerPlace = {
    lng: 0,
    lat: 0
  }

  @Input() to = {
    lng: 0,
    lat: 0
  };
  @Input() centeredIn = {
    lng: 0,
    lat: 0
  };
  
  @Input() ridersIds = []

  public mapStyles:any = [
    {
    "featureType": "poi",
    "elementType": "labels",
    "stylers": [
      {
        "visibility": "off"
      },
      
      ]
    },
    {
      featureType: "transit",
      elementType: "labels.icon",
      stylers: [{ visibility: "off" }],
    },
  ]!;

  public lastPoint:any = {
    lat: 0,
    lng: 0,
  };
  public mapOptions:MapOptions = {};
  public storeLocation:any = {
    lat : -33.405983,
    lng : -70.569927
  }
  public pathOnHover = null;


  constructor(public ridersService:RidersService , public trackingService:TrackingService, public ordersService:OrdersService, public mapService:MapService) {
    this.centeredIn = {
      lat : -33.405983,
      lng : -70.569927
    }
  }

  ngOnInit(): void {
    this.mapService.mapOptions$.subscribe(mapOptions => {
      this.mapOptions = mapOptions
    })
  }

  private distance(location1:any,location2:any){
    return Math.sqrt(Math.pow(location1.lat - location2.lat,2) + Math.pow(location1.lng - location2.lng,2))
  }

  getPassedLines(riderLocation:any){
    let minDistance = 0.1;
    let minDistanceIndex = 0;
    if(!this.polyline){
      return
    }
    this.polyline.forEach((point, index)=>{
      const distance = this.distance(riderLocation,{lat:point[0],lng:point[1]})
      if(distance < minDistance){
        minDistance = distance;
        minDistanceIndex = index;
      }
    })
    if(minDistanceIndex != 0){
      return this.polyline.splice(0,minDistanceIndex)
    }
    if(this.polyline[this.polyline.length-1]){
      const lat = this.polyline[this.polyline.length-1][0]
      const lng = this.polyline[this.polyline.length-1][1]
      const type = (this.lastPoint.lat.toFixed(3) == this.storeLocation.lat.toFixed(3)) ? "store" : "place";
      this.lastPoint = { lat: lat, lng : lng, type: type}
      console.log(this.lastPoint.lat.toFixed(3),this.storeLocation.lat.toFixed(3), lat,lng)
    }


    return this.polyline
  }

  public getRiderLocation(rider:any):any{
    return rider.location
  }

  public getLatitude(rider:any){
    if(rider.location){
      return rider.location.latitude
    }
    else{
      return this.storeLocation.lat
    }
  }

  public onchange(event:any){
    if(event.routes){
      console.log(typeof event.routes[0].overview_path)
      const paths = event.routes[0].overview_path;
    }
  }

  
  public getColor(key:any){
    if(this.trackingService.fillColors[key]){

      return this.trackingService.fillColors[key]
    }
    else{
    }
  }

  public toAny(item:any):any{
    return item
  }

  public getLongitude(rider:any){
    console.log(rider)
    if(rider.location){
      return rider.location.longitude
    }
    else{
      return this.storeLocation.lng
    }
  }

  public overRider(rider:any){
    if(rider.currentOrderId){
      return this.ordersService.orders.find((order:any)=>{
        return order.id == rider.currentOrderId
      })
    } else {
      return null
    }
  }
  

}
