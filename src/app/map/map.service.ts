import { Injectable } from '@angular/core';
import { IsochronusOptions } from './map.interfaces';
import { MapOptions } from './map.interfaces';
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class MapService {

  private isochronusOptions:IsochronusOptions = {
    hidden : false,
  }

  public mapOptions:MapOptions = {
    isochronus : {
      hidden: true
    },

    stores : {
      hidden: false
    },

    places : {
      hidden: false
    },
    
    riders : {
      hidden: false
    }
  };

  public routes = false;

  private mapOptionsSubjectBehavior:BehaviorSubject<MapOptions> = new BehaviorSubject<MapOptions>(this.mapOptions)
  public mapOptions$:Observable<MapOptions> = this.mapOptionsSubjectBehavior.asObservable();

  public editOption(options:MapOptions){
    this.mapOptions = { ...this.mapOptions, ...options }
    console.log(this.mapOptions, options)
    this.mapOptionsSubjectBehavior.next(this.mapOptions)
  }

  constructor() {
  }
}
