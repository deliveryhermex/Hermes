import { Observable, Subject } from "rxjs";
import { SocketNameSpace } from "../services/hermes-socket.service";

export interface fetcher{
    model: string,

    
    //Api
    where?: any,
    limit?: number,
    include?: string,
        
    //Socket
    socket?: SocketNameSpace
    
    
    //Observers
    currentData?:Subject<any>
    newObject?: Observable<any>
    removedObject?: Observable<any>
    
    //Data
    data?: any
    error?: any
    
   
    currentPage?: number,
    nextPage?: number,
    totalPages?: number,
}