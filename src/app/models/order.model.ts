import { Rider } from "./rider.model";
import { status } from "./status.model";
import { Store } from "./store.model";

export interface Order{
    id : string,
    refId: string,

    currentStatus?: status,
    currentStore?: Store,
    currentRider?: Rider,

    statuses?: Array<status>
}