import { Order } from "../services/orders.interfaces";
import { Bag } from "./bag.model";
import { status } from "./status.model";
import { Store } from "./store.model";

export interface Rider{
    id: string

    bag?: Bag

    currentOrder?: Order
    currentStatusId?: status
    currentStore?: Store

    timeToStore?: number
    timeToCustomer?: number
}