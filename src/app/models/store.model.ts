import { Order } from "../services/orders.interfaces";
import { fetcher } from "./fetcher.model";
import { Rider } from "./rider.model";

export interface Store{
    id:string

    currentRiders? : Array<Rider>
    currentOrders? : Array<Order>

    ordersDelivered?: fetcher,
    ordersInQueue?: fetcher,
    ordersInDelivery?: fetcher 
}