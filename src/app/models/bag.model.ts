import { Order } from "../services/orders.interfaces";
import { Rider } from "./rider.model";

export interface Bag{
    id: string,

    rider: Rider,
    orders: Array<Order>
}