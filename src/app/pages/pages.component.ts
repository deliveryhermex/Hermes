import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';
import { Router } from '@angular/router';
import { NavigationEnd } from '@angular/router';
@Component({
  selector: 'app-pages',
  templateUrl: './pages.component.html',
  styleUrls: ['./pages.component.scss']
})
export class PagesComponent implements OnInit {
  public hideToolbar: boolean = false;
  
  constructor(private router:Router, public auth:AuthService){
    router.events.subscribe(event=>{
      if(event instanceof NavigationEnd){
        if(!auth.credentials.signed){

          this.router.navigateByUrl("/login")
        }
        if(event.url.split("/").length == 3 && event.url.split("/")[1] == "riders"){
          this.hideToolbar = false;
        } else {
          this.hideToolbar = false;
        }
      }
    })
  }

  public actions = [
    {
      name:"Dashboard",
      icon: "dashboard",
      route: ""
    },
    {
      name: "Orders",
      icon: "inventory_2",
      route: "orders"
    },
    {
      name:"Darkstores",
      icon: "storefront",
      route: "darkstores"
    },
  {
    name: "Mapa",
    icon: "map",
    route: "map"
  },
  {
    name: "Riders",
    icon: "directions_bike",
    route: "riders"
  },
]

  title = 'hermes';
  ngOnInit(): void {
  }

}
