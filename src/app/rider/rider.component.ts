import { Component, OnInit } from '@angular/core';
import { OrdersService } from 'src/app/services/orders.service';
import { RidersService } from 'src/app/services/riders.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-rider',
  templateUrl: './rider.component.html',
  styleUrls: ['./rider.component.scss']
})
export class RiderComponent implements OnInit {

  public stateNameToDescription:any = {
    "queued" : "El pedido esta siendo asignado a un rider",
    "assigned": "El pedido ha sido asignado a un rider",
    "accepted": "El pedido ha sido aceptado por un rider",
    "near store": "El rider esta cercano a la Lomi store",
    "picked": "El rider esta llevando el pedido a su destino",
    "near dropoff": "El rider esta cercano al lugar de destino",
    "dropoff": "El rider ha entregado el pedido"
  }

  public ratingToArray(rating:any){
    const rateJson:any = {rating: rating, array: Array(5).fill(0)};
    for(var i = 0; i < rating; i++){
      rateJson.array[i] = 1
    }
    return rateJson
  }

  public order:any;
  public initialLocation = {
    lat:0,
    lng:0
  }

  public storeLocation:any = {
    lat : -33.405983,
    lng : -70.569927
  }

  public events = [
    ["Queued", "12:08"],
    ["Dispatched", "12:09"],
    ["Courier notified", "12:09"],
    ["Accepted", "12:10"],
    ["Near pickup", "12:14"],
    ["Picked up", "12:14"],
    ["Near dropoff", "12:20"]
   ]

  public rider:any = {
    name: "Antony Eduardo Torres",

    pedidos: 5,
    avgTime: 9,

    

    events: [
     ["Queued", "12:08"],
     ["Dispatched", "12:09"],
     ["Courier notified", "12:09"],
     ["Accepted", "12:10"],
     ["Near pickup", "12:14"],
     ["Picked up", "12:14"],
     ["Near dropoff", "12:20"]
    ]
  }


  public displayedColumns = [
    "event","time","estimatedAccept","estimatedArrival","estimatedDeparture","estimatedDropoffArrival", "estimatedDropoffLeave" 
  ]

  public eventsDetails = {
    "Queued" : 0, "Dispatched" : 1, "Courier notified" : 2, "Accepted" : 3, "Near pickup" :4, "Near dropoff": 5, "Completed": 6
  }


  public belowEvent(event:String, number:number){
    if(number >= 3){
      if(event == "Queued" || event == "Dispatched" || event=="Courier notified")return true
    }
    if (number > 4){
      if(event == "Accepted")return true;
    }
    if(number > 5){
      if(event == "Near pickup")return true;
    }
    if(number > 6){
      if(event == "Picked up")return true;
    }
    if(number > 7){
      if(event == "Near dropoff")return true
    }
    return false
  }

  public plusTime(time : string, plus: number){
    let [hour, minutes] = time.split(":")
    let newHour, newMinutes
    if (parseInt(minutes) + plus > 59){
      newHour = parseInt(hour) + 1;
      newMinutes = (parseInt(minutes) + (plus)) % 60
    }
    else{
      newHour = hour
      newMinutes = parseInt(minutes) + plus
    }
    return newHour+":"+newMinutes
  }

  fetchRider(){
    const i = this.riders.fleet.findIndex((rider:any) => rider.name == name);
    if(i != -1){
      const rider = this.riders.fleet[i]
      this.rider.events = this.rider.events;
      this.rider = rider;
      if(!this.initialLocation.lat){
        this.initialLocation = this.rider.location
      }
      console.log(this.rider)
    }
  }
  
  constructor(private router:Router, public riders:RidersService, public orders:OrdersService) {
    const url = this.router.url.split("/")
    const name = decodeURI(url[url.length - 1])
    console.log(this.riders.fleet)
    
    const i = this.riders.fleet.findIndex((rider:any) => rider.name == name);
    if(i != -1){
      const rider = this.riders.fleet[i]
      this.rider.events = this.rider.events;
      this.rider = rider;
      console.log(this.rider)
    }
    this.fetchOrder();
    setInterval(()=>{
      this.fetchOrder();
      
      const i = this.riders.fleet.findIndex((rider:any) => rider.name == name);
      if(i != -1){
        const rider = this.riders.fleet[i]
        this.rider.events = this.rider.events;
        this.rider = rider;
        console.log(this.rider)
      }
    },2000)
  }

  public basketValue(items:any){
    let sum = 0
    items.forEach((item:any) => {
      sum += item.price * item.quantity;
    });
    return sum;
  }
  
  
  fetchOrder = () => {
    const currentOrderId = this.rider.currentOrder
    if(!this.rider || !this.orders.orders || !currentOrderId){
      return
    }
    const i = this.orders.orders.findIndex((order:any)=>{
      return order.id == currentOrderId
    })
    if(i != -1){
      this.order = this.orders.orders[i]
    } else {
      console.log("No order with that rider", currentOrderId)
    }
  }
  ngOnInit(): void {
  }

}
