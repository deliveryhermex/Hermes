import { Component, Input, OnInit, OnDestroy } from '@angular/core';
import { OrdersService } from 'src/app/services/orders.service';
import { RidersService } from 'src/app/services/riders.service';
import { fetcher } from '../models/fetcher.model';
import { StoresService } from '../services/stores.service';
import { TrackingService } from '../services/tracking.service';

@Component({
  selector: 'app-orders',
  templateUrl: './orders.component.html',
  styleUrls: ['./orders.component.scss']
})
export class OrdersComponent implements OnInit, OnDestroy {

  @Input() storeId:number = 0;

  public displayedColumns:any = []
  public pageSizeOptions: number[] = [5, 10, 25, 50, 100];
  public selectedPage = 0;
  public pageSize = 20;
  public changingRiderOrder:any;
  public changingStackingOrder:any;
  public loading:boolean = false;
  private refreshOrdersInterval;

  public fetcher:fetcher | undefined;

  get orders(){
    return this.ordersService.fetcher?.data
  }

  constructor(public trackingService:TrackingService, public storesService:StoresService, public ordersService:OrdersService, public ridersService:RidersService) {
<<<<<<< HEAD
=======
    this.refreshOrders()
    this.refreshOrdersInterval = setInterval(()=>{
      this.refreshOrders();
    },5000)
>>>>>>> old-console
  }

  ngOnInit(): void {  
    this.displayedColumns = !this.storeId ? 
    [
<<<<<<< HEAD
      "name","store","refId", "queued","dispatched", "active", "delay","minutes","work", "rider","stacking","actions"
=======
      "name","store","refId", "queued","dispatched", "active", "delay","minutes","work", "rider", "actions"
>>>>>>> old-console
    ] : [
      "refId","minutes","work", "rider"
    ]
  }

<<<<<<< HEAD
=======
  ngOnDestroy(){
    clearInterval(this.refreshOrdersInterval)
  }


  public refreshOrders(){
    this.ordersService.fetchOrders().then((orders)=>{
      this.orders = orders;
      this.orders.data.rows = this.storeId? this.orders.data.rows.filter((order:any)=>{
        return order.storeId == this.storeId
      }): orders.data.rows;
      console.log(this.orders.data.rows,"rows")
      this.orders.data.rows = this.orders.data.rows.length >= 5 && this.storeId ? this.orders.data.rows.slice(0,5) : this.orders.data.rows
    }
  )
  
  }
>>>>>>> old-console
  public assignRider(orderId:number, riderId:number){
    this.ordersService.changeState(orderId, 2,riderId)
    this.changingRiderOrder = null
  }

  public paginatorEvent(event:any){
    this.ordersService.pageSize = event.pageSize
    this.selectPage(event);
  }

  public changeRiderOrder(order:any){
    if(this.changingRiderOrder == order.id){
      this.changingRiderOrder = null;
      return
    }
    if(order.status.id < 8){
      this.changingRiderOrder = order.id;
    } else {
      return
    }
  }

  public changeStackingOrder(order:any){
    if(this.changingStackingOrder == order.id){
      this.changingStackingOrder = null;
      return
    }
    if(order.status.id < 8){
      this.changingStackingOrder = order.id;
    } else {
      return
    }
  }
  public paginated(data:Array<any>){
    let ret = data.slice(this.pageSize*this.selectedPage, this.pageSize*this.selectedPage + this.pageSize)
    return ret
  }

  public selectPage(event:any){
    this.loading = true;
    this.ordersService.setPage(event.pageIndex).then((orders)=>{
      this.loading = false;
      console.log("new orders", orders,event.pageIndex);
    })
  }
}
