import { Component, OnInit,Input, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { OrdersService } from 'src/app/services/orders.service';
import { RidersService } from 'src/app/services/riders.service';
import { TrackingService } from 'src/app/services/tracking.service';

@Component({
  selector: 'app-order',
  templateUrl: './order.component.html',
  styleUrls: ['./order.component.scss']
})
export class OrderComponent implements OnInit, OnDestroy {

  @Input() order:any;
  public rider:any;
  public state:any;
  private refreshOrderInterval;

  constructor(private trackingService:TrackingService, private router:Router, public riders:RidersService, public orders:OrdersService) {
    const url = this.router.url.split("/")
    const orderId = decodeURI(url[url.length - 1])
    this.refreshOrderInterval = setInterval(()=>{
      orders.getOrderById(orderId).then((order)=>{
        this.order = order;
        let riderLocationIndex = trackingService.getRiderLocationIndex(this.order.riderId);
        this.rider =  riderLocationIndex != -1 ? trackingService.tracked[riderLocationIndex] : null
      });
    },1000)
    orders.getOrderById(orderId).then((order)=>{
      this.order = order;
      let riderLocationIndex = trackingService.getRiderLocationIndex(this.order.riderId);
      this.rider =  riderLocationIndex != -1 ? trackingService.tracked[riderLocationIndex] : null
    });
  }
  public getMinutes(){
    return (((new Date().getTime() - this.order.id) / 1000 / 60).toFixed())
  }

  get orderLocation(){
    return { lat: this.order.customer.latitude , lng: this.order.customer.longitude }
  }

  get storeLocation(){
    if(this.order?.storeId){
      const trackingService = this.trackingService.getTrackingByStoreId(this.order.storeId)
      return trackingService.storeLocation;
    }
  }

  ngOnInit(): void {
  }

  ngOnDestroy(){
    clearInterval(this.refreshOrderInterval)
  }

}
