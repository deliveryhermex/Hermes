import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DarkstoresComponent } from './darkstores.component';

describe('DarkstoresComponent', () => {
  let component: DarkstoresComponent;
  let fixture: ComponentFixture<DarkstoresComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DarkstoresComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DarkstoresComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
