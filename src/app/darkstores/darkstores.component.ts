import { Component, OnInit } from '@angular/core';
import { OrdersService } from 'src/app/services/orders.service';
import { RidersService } from 'src/app/services/riders.service';
import { StoresService } from '../services/stores.service';
import { TrackingService } from '../services/tracking.service';

@Component({
  selector: 'app-darkstores',
  templateUrl: './darkstores.component.html',
  styleUrls: ['./darkstores.component.scss']
})
export class DarkstoresComponent implements OnInit {

  public displayedColumns = [
    "name", "queued","dispatched", "active", "delay",  "start","work", "temp", "break", "rider"
  ]

  public stores = [
    "Las Condes"
  ]

  public tiendas = [
    { 
      name: "Las Condes",
      id: 1,
      queued: 0,
      dispatched: 0,
      active: 9,
      delay: 0,
      temp: 0,
      break: 0,
      work: 0,
      start: 0
    },
    {
      name: "Huechuraba",
      id: 2,
      queued: 19,
      dispatched: 0,
      active: 21,
      delay: 5,
      temp: 0,
      break: 0,
      work: 39,
      start: 11
    },
    { 
      name: "Lo Barnechea",
      id: 3,
      queued: 10,
      dispatched: 1,
      active: 22,
      delay: 5,
      temp: 0,
      break: 0,
      work: 34,
      start: 10
    },
    { 
      name: "Providencia",
      id: 4,
      queued: 4,
      dispatched: 2,
      active: 16,
      delay: 5,
      temp: 0,
      break: 0,
      work: 23,
      start: 8
    },
    { 
      name: "Peñalolen",
      id: 5,
      queued: 10,
      dispatched: 0,
      active: 22,
      delay: 5,
      temp: 0,
      break: 0,
      work: 34,
      start: 10
    },
    { 
      name: "Santiago Centro",
      id: 6,
      queued: 85,
      dispatched: 116,
      active: 730,
      delay: 7,
      temp: 3,
      break: 25,
      work: 920,
      start: 549
    }
  ]

  constructor(public trackingService:TrackingService, public storesService:StoresService , public ordersService:OrdersService, public ridersService:RidersService) {
    console.log(trackingService.trackingStatus);
    const tienda = { 
      name: "Las Condes",
      id: 1,
      queued: 0,
      dispatched: 0,
      active: 0,
      delay: 0,
      temp: 0,
      break: 0,
      work: 0,
      start: 0
    }
  }

  ngOnInit(): void {
  }

}
