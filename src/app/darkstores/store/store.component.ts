import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { OrdersService } from 'src/app/services/orders.service';
import { RiderComponent } from 'src/app/rider/rider.component';
import { RidersService } from 'src/app/services/riders.service';
import { TrackingService } from 'src/app/services/tracking.service';
import { StoresService } from 'src/app/services/stores.service';
import { ActivatedRoute } from '@angular/router';
@Component({
  selector: 'app-store',
  templateUrl: './store.component.html',
  styleUrls: ['./store.component.scss']
})
export class StoreComponent implements OnInit {

  public store:any;
  public storeTracking:any;

  constructor(private route:ActivatedRoute, public trackingService:TrackingService, public storesService:StoresService, public dialog: MatDialog, public ridersService:RidersService, public ordersService:OrdersService) {
    console.log(this.route.queryParams,"QueryParams");

  }

  openDialog() {
    const dialogRef = this.dialog.open(RiderComponent);

    dialogRef.afterClosed().subscribe(result => {
      console.log(`Dialog result: ${result}`);
    });
  }

  ngOnInit(): void {
    this.route.params.subscribe(params=>{
      let storeId = params.store
      this.store = this.storesService.getStoreById(storeId);
      this.storeTracking = this.trackingService.getTrackingByStoreId(storeId);
      console.log(this.storeTracking,"storeTracking");
    })
  }

}
