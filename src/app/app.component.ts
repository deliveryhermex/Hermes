import { Component, OnInit } from '@angular/core';
import { NavigationEnd, Router, RouterEvent } from '@angular/router'
import { AuthService } from './auth.service';
import { MatSidenav } from '@angular/material/sidenav';
<<<<<<< HEAD
import { HermesSocketService } from './services/hermes-socket.service';
=======
import {MatBottomSheet, MatBottomSheetRef} from '@angular/material/bottom-sheet';
import { OrdersService } from './services/orders.service';
import { StoresService } from './services/stores.service';

@Component({
  selector: 'bottom-sheet-add-order',
  templateUrl: './bottom-sheet-add-order.html',
})
export class BottomSheetAddOrder implements OnInit {
  orderId:any = "";
  public order: any = {
    ship_address: {
      full_name: "",
      address1 : "",
      address2 : "",
      phone: ""
    },
    shipments : [
      {
        stock_location_name: ""
      }
    ]
  };

  ngOnInit(): void {
      
  }

  constructor(public stores:StoresService, private _ordersService:OrdersService, private _bottomSheetRef: MatBottomSheetRef<BottomSheetAddOrder>) {}

  submitOrder(){
    if(!this.orderId || !this.order.ship_address.full_name || !this.order.ship_address.address1 || !this.order.shipments.length){
      return
    }
    let store:any = this.stores.getStoreByName(this.order.shipments[0].stock_location_name)
    let order = {
        "orderId": this.orderId,
        "customer": {
            "name": this.order.ship_address.full_name,
            "phone": this.order.ship_address.phone ,
            "address1" : this.order.ship_address.address1,
            "address2": this.order.ship_address.address2
        },
        "store": {
            "address1": this.order.shipments[0].stock_location_name,
            "stockLocationId": store.id,
            "phone": "+569982942818"
        }
    }
    this._ordersService.newOrder(order);
  }

  dismiss(): void {
    this._bottomSheetRef.dismiss();
  }

  getOrder(orderInput:any){
    this.orderId = orderInput.value;
    this._ordersService.getOrderByRefId(orderInput.value).then((order)=>{
      this.order = order;
      console.log(order,this.order);
    })
  }


}
>>>>>>> old-console

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

  public hideToolbar = false;

<<<<<<< HEAD
  constructor(private router:Router, private socket:HermesSocketService, public auth:AuthService){
=======
  constructor(private _bottomSheet:MatBottomSheet, private router:Router, public auth:AuthService){
>>>>>>> old-console
    router.events.subscribe(event=>{
      if(event instanceof NavigationEnd){
        if(!auth.credentials.signed){

          this.router.navigateByUrl("/login")
        }
        if(event.url.split("/").length == 3 && event.url.split("/")[1] == "riders"){
          this.hideToolbar = false;
        } else {
          this.hideToolbar = false;
        }
      }
    })
  }

  addOrderSheet(){
    this._bottomSheet.open(BottomSheetAddOrder)
  }

  public actions = [
    {
      name: "Pedidos",
      icon: "inventory_2",
      route: "orders"
    },
  {
    name: "Mapa",
    icon: "map",
    route: "map"
  }
]

  title = 'hermes';
}
