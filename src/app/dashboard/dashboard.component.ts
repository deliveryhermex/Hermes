import { Component, OnInit } from '@angular/core';
import { ChartType } from 'angular-google-charts';
import { OrdersService } from 'src/app/services/orders.service';
import { RidersService } from 'src/app/services/riders.service';
import { TrackingService } from '../services/tracking.service';
import { StoresService } from '../services/stores.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

  constructor(public stores:StoresService, public trackingService: TrackingService, public orders:OrdersService, public riders:RidersService) {
    setInterval(this.updateCharts,2000)
  }

  updateCharts= () => {
    this.deliveryData = [
      ["Activos" , this.trackingService.trackingStatus ? this.trackingService.merged.ordersInQueue.length: 0],
      ["Disponibles" , this.trackingService.trackingStatus ? this.trackingService.merged.avaibleRiders.length : 0],
      ["Repartiendo" , this.trackingService.trackingStatus ? this.trackingService.merged.deliveringRiders.length : 0],
      ["En break" , this.trackingService.trackingStatus ? this.trackingService.merged.avaibleRiders.length : 0],
    ]
  }

  getTotalOrdersInCourse(){
    return Object.values(this.trackingService.trackingStatus).reduce((prevStatus:any, nextStatus:any)=>{
      if(typeof prevStatus == typeof {}){
        return prevStatus.deliveringRiders.length + prevStatus.avaibleRiders.length + nextStatus.deliveringRiders.length + nextStatus.ordersInQueue.length
      } else if(!nextStatus && prevStatus) {
        return 0
      } else {
        return prevStatus + nextStatus.deliveringRiders.length + nextStatus.ordersInQueue.length
      }
    })
  }

  deliveryData:any = [


  ]

  public ordersData = [
    [11,20, 2],
    [12,22, 7],
    [13,32, 8 ],
    [14,27, 2]
  ]

  columns = [
    'Tipo', 'Cantidad' 
  ]

  barType:ChartType = ChartType.Bar
  lineType:ChartType = ChartType.LineChart

  ngOnInit(): void {
  }

}
