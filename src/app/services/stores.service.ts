import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { storesSchedules } from './riders-schedule'

const SPREE_URL = "https://lomi.cl/api/v2/storefront"
const SPREE_TOKEN = "8b9c307dd89928cc60e8e59d2233dbafc7618f26c52fa5d3"

@Injectable({
  providedIn: 'root'
})
export class StoresService {

  public stores = [];

  constructor(private http:HttpClient) {
    this.fetchData();
  }

  getStoreById(storeId:any):any{
    if(!storeId)return
    let store:any = this.stores.find((store:any)=>store.id == storeId);
    return store;
  }

  fetchData = () => {
    this.getStores();
  }

  getStoreByName(name:any){
    return this.stores.find((store:any)=> store.name == name)
  }

  getStores = () => {
    this.http.get(environment.hermexApiUrl+"/stores").subscribe((res:any)=>{
      console.log(res)
      this.stores = res
    })
  }

  getRidersFromStore(storeId:number){
    return storesSchedules.find((store)=>store.id==storeId)
  }
}
