import { Injectable } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Socket, SocketIoConfig } from 'ngx-socket-io';
import { environment } from 'src/environments/environment';

export class SocketNameSpace extends Socket{
  constructor(socketConfig: SocketIoConfig){
    super(socketConfig);
  }
}


@Injectable({
  providedIn: 'root'
})
export class HermesSocketService {

  private orders:SocketNameSpace;
  private riders:SocketNameSpace;
  
  private nameSpaces:any = {}


  constructor(private socket: Socket, private snackBar:MatSnackBar) {
    
    this.orders = new SocketNameSpace({
      url: environment.socketUrl+"/orders"
    })

    this.riders = new SocketNameSpace({
      url: environment.socketUrl+"/riders"
    })

  }

  public onNewOrder(callback : any){
    this.socket.on('newOrder',(order:any)=>{
      this.snackBar.open('Nueva orden' + order.id)
      callback(order)
    })
  }
  
  private riderLocation(callback: any){
    this.socket.on('riderUpdate',callback)
  }

  private anyMessage(callback: any){
    this.socket.on('message',callback)
  }

}
