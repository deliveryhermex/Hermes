import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { fetcher } from '../models/fetcher.model';
import { HttpClient } from '@angular/common/http';
import { Observable, Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})

export class FetcherService {

  async fetchObject(fetcher:fetcher){
    
    fetcher.currentData = new Subject();
    
    fetcher.currentPage = fetcher.currentPage? fetcher.currentPage : 0;
    fetcher.totalPages = undefined;
    fetcher.nextPage = undefined;
    fetcher.limit = fetcher.limit? fetcher.limit: 25;
    fetcher.include = fetcher.include? fetcher.include: "";
    fetcher.where = fetcher.where? JSON.stringify(fetcher.where) : "";
    
    let fetcherUrl = 
      environment.hermexApiV2Url + "/" +
      fetcher.model + "?page=" +
      fetcher.currentPage + (fetcher.limit? "&limit=" : "") +
      fetcher.limit + (fetcher.where? "&where=" : "") +
      fetcher.where + (fetcher.include? "&include=": "") +
      fetcher.include + ""

      console.log("Fetching", fetcherUrl,fetcher)
      this.http.get(fetcherUrl).subscribe((res:any)=>{
        if(res?.data.rows){
          fetcher.data = res?.data
          console.log("Nexting",res?.data)
          fetcher.currentData?.next(res?.data)
        } else {
          fetcher.data = res?.data
        }
      },
      
      (err)=>{
        fetcher.error = err.error;
      });
    return fetcher;
  }

  listenToNewObject(fetcher:fetcher, callback:Function){
    return fetcher.socket?.on("new",callback)
  }

  listenToRemovedObject(fetcher:fetcher, callback:Function){
    return fetcher.socket?.on("remove",callback)
  }

  constructor(private http:HttpClient) { }

}
