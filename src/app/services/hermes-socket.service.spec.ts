import { TestBed } from '@angular/core/testing';

import { HermesSocketService } from './hermes-socket.service';

describe('HermesSocketService', () => {
  let service: HermesSocketService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(HermesSocketService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
