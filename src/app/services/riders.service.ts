import { Injectable } from '@angular/core';
import { Customer } from './orders.interfaces';
import { Order } from './orders.interfaces';
import { Socket } from 'ngx-socket-io';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { combineLatest } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class RidersService {

  lat = -33.405983
  lng = -70.569927

  //Must be an observable
  public fleet:any = [];


  public riders:any = [

    /**
    { name: "Edga Urbina" , state: "Working", img:"assets/pictures/5.png", rating: 4, deliveryTime: 7, orderPerHour: 5},
    { name: "Antony Eduardo Torres" , state: "Working",img:"assets/pictures/2.png", rating:3 , deliveryTime: 10, orderPerHour: 4},
    { name: "Antonia chirinos" , state: "Working",img:"assets/pictures/1.png", rating:4, deliveryTime: 6, orderPerHour: 8},
    { name: "Jhoander Gonzalez" , state: "Working", img:"assets/pictures/6.png", rating: 4, deliveryTime: 7, orderPerHour: 6},
    { name: "Nicol Alzate Rodriguez" , state: "Working", img:"assets/pictures/4.png", rating:5, deliveryTime: 5, orderPerHour: 10},
    { name: "Carlos Brito" , state: "Temp not working", img:"assets/pictures/3.png", rating:3, deliveryTime: 9, orderPerHour: 6},
    { name: "Jhan carlos Sanchez colmenares" , state: "Break", img:"assets/pictures/7.png", rating:2, deliveryTime: 12, orderPerHour: 4},
    { name: "Francisco Velasquez" , state: "Starting", img:"assets/pictures/8.png", rating:1, deliveryTime: 15, orderPerHour: 3},
    { name: "Andre Velasquez" , state: "Starting", img:"assets/pictures/9.png", rating: 3, deliveryTime: 9, orderPerHour: 4},
    */
  ]

  getRiders(){
    return this.http.get(environment.hermexApiUrl+"/riders");
  }

  getDeliveringOrders(){
    return this.http.get(environment.hermexApiUrl+"/api/orders?limit=100&search=on_delivery_orders_only:true;")
  }

  getRider(id:any){
    console.log("Getting rider",id)
    let rider = this.riders.find((rider:any)=> rider.id == id);
    return rider? rider : { name: "Andre Velasquez" , state: "Starting", img:"assets/pictures/9.png", rating: 3, deliveryTime: 9, orderPerHour: 4}
  }

  constructor(private http:HttpClient, private socket:Socket) {
    this.getRiders().subscribe((data:any)=>{
      this.riders = data.filter((rider:any)=>{
        return rider.id > 8 && rider.id != 10 && rider.id != 12
      })
      console.log(this.riders)
    })
    
    combineLatest([
      this.getRiders(),
      this.getDeliveringOrders(),
    ]).subscribe((data:any)=>{
      let [riders, orders] = data;
      orders = orders.data.rows.map((order:any)=>{
        return { riderId: order.riderId, currentStatus: order.status.id , order:order}
      })
      console.log(riders,orders);
    })


    socket.emit("getRiders", null, (riders:any)=>{
      this.fleet = riders
      this.riders = riders
      console.log("[Getting Riders]",this.riders)
    })
    socket.on("updateRiders",(riders:any)=>{
      this.fleet = riders
      this.riders = riders
    })
    return
    for(let i=0;i<this.riders.length;i++){
      const ordenes = Math.floor(Math.random()*500)
      const speed = Math.floor(Math.random()*20 )/100+0.1

      const outOfTime = ((Math.floor(Math.random()*ordenes) * Math.floor(Math.random()*10))/100).toFixed()
      const maxTime = Math.floor(Math.random() * 10) + 10
      const rating = Math.floor(Math.random() * 5)

      const factor = Math.floor(Math.random() * 30)/100 + 0.85

      this.riders[i].deliveryTime = parseInt((60*speed).toString())
      

      this.riders[i].description = {
        "Nombre" : this.riders[i].name,
        "Ordenes totales" : ordenes,
        "Fecha inicio" : new Date('2020-09-31').toLocaleDateString(),
        "Total horas trabajadas" : parseInt((ordenes*speed).toString()),
        "Total dinero ganado" : ordenes*3500,
      }

      this.riders[i].stats = [
        {name: "Ordenes fuera de tiempo", q: outOfTime + " ordenes", icon: "inventory"},
        {name: "Tiempo maximo de entrega", q: maxTime + " minutos", icon: "timer"},
        {name: "CPO", q: factor*3500+" Clp", icon: "attach_money"},
      ]

      this.riders[i].rating = rating
    }
    return
    for(let i=0;i<this.riders.length;i++){
      this.riderOnDuty()
    }
  }

  public getAvaibleRiders():Array<any>{
    return this.fleet.filter((rider:any)=>{
      return rider.state == "avaible"
    })
  }

  private distance(location1:any,location2:any){
    return Math.sqrt(Math.pow(location1.lat - location2.lat,2) + Math.pow(location1.lng - location2.lng,2))
  }

  public getNearestRider(location:any = {lat:this.lat, lng:this.lng}){
    let distanceFromStore = 1;
    if(!this.getAvaibleRiders().length){
      return
    }
    let selectedRider = this.getAvaibleRiders()[0];
    this.getAvaibleRiders().forEach((rider:any) => {
      let newDistanceFromStore = this.distance(rider.location,location);
      if(distanceFromStore > newDistanceFromStore){
        distanceFromStore = newDistanceFromStore
        selectedRider = rider
      }
    });
    selectedRider.rider.state = "dispathed"
    return selectedRider
  }
  
  private riderOnDuty = () => {
    const storeLat = -33.405983
    const storeLon = -70.569927

    const riderIndex = Math.floor(Math.random()*this.riders.length)

    let rider:any = {
      location: {
        lat: storeLat + (Math.floor(Math.random()*10) % 2 ? 1 : -1)*(Math.floor(Math.random() * 150)) / 10000,
        lng: storeLon + (Math.floor(Math.random()*10) % 2 ? 1 : -1)*(Math.floor(Math.random() *150)) / 10000,
      },
      rider: this.riders[riderIndex],
    }

    console.log("[Rider]", rider)
    rider.rider.state = "Avaible"
    this.fleet.push(rider)
    this.riders.splice(riderIndex,1)
  }
}
