import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';


const API_URL = environment.hermexApiUrl
const OR_URL = "https://api.openrouteservice.org/v2/isochrones/cycling-electric"
const headers = {
  "Authorization": "5b3ce3597851110001cf6248de487597ed9e440791a78e62c8dab777"
}
const body = {
  "locations":[[-70.569927,-33.405983]],
  "range":[60*8,60*5,60*3,60],
  "attributes":["reachfactor"],
  "range_type":"time"
}

@Injectable({
  providedIn: 'root'
})
export class TrackingService {
  
  //Must be an observable
  public tracked:any = []
  public trackingStatus:any = [];

  private bearerToken = ""
  public heatMaps:Array<any> = []
  public renderedHeatMaps:any = {}

  public renderOptions:any = {
    suppressMarkers: true,
  }

  public renderOptionsGreen:any = {
    suppressMarkers: true,
    polylineOptions: { strokeColor: '#0f0' }
  }

  public markerOptions = {
    destination: {
      icon: "assets/markers/place.png",
    },
  }

  public heatRanges = [60*8,60*5,60*3,60]

  public fillColors:any = {
    60 : "#75CFB8",
    180 : "#75CFB8",
    300 : "#75CFB8",
    480 : "#FFC478",
  }

  constructor(private http:HttpClient) {
    this.fetchData();
    setInterval(this.fetchData,5000);

    this.getHeatMap()
  }

  public directionWithLineColor(color:string):any{
    const options =  {
      suppressMatkers: true
    }

    return options
  }

  getHeatMap(){
    this.http.post(OR_URL,body,{headers:headers}).subscribe((res:any)=>{
      if(res){
        this.heatMaps = res.features.forEach((heatMap:any)=>{
          this.renderedHeatMaps[heatMap.properties.value] = []
          heatMap.geometry.coordinates[0].forEach((coordinate:any)=>{
            this.renderedHeatMaps[heatMap.properties.value].push({lat:coordinate[1],lng:coordinate[0]})
          })
        })
      }
    })

  }

  get merged(){
    let reduced = this.trackingStatus ? this.trackingStatus.reduce((prev:any,next:any)=>{
      return prev ? { 
        avaibleRiders: [...prev.avaibleRiders, ...next.avaibleRiders], 
        deliveringRiders: [...prev.deliveringRiders, ...next.deliveringRiders], 
        ordersInQueue: [...prev.ordersInQueue, ...next.ordersInQueue] 
      } : { 
        avaibleRiders: [...next.avaibleRiders], 
        deliveringRiders: [...next.deliveringRiders], 
        ordersInQueue: [...next.ordersInQueue] 
      }
    }) : {}
    return reduced;
  }

  getMinutesFromLastLocation(lastLocationTime:number){
    const diff = new Date().getTime() - lastLocationTime;
    return parseInt((diff/60/1000).toString())
  }

  fetchData = () => {
    this.getTrackingStatus();
    this.getFleet();  
  }

  getTrackingStatus = async () => {
    this.http.get(environment.hermexApiUrl+"/trackingServiceStatus").subscribe((res:any)=>{
      this.trackingStatus = res;
    })
  }

  getRiderLocation(riderId:number){
    return this.tracked.find((rider:any)=>rider.id == riderId)
  }
  
  getRiderLocationIndex(riderId:number){
    return this.tracked.findIndex((rider:any)=>rider.id == riderId)

  }

  getTrackingByStoreId(storeId:number){
    return this.trackingStatus? this.trackingStatus.find((trackingStatus:any)=>trackingStatus.storeId == storeId) : null
  }

  getFleet = () => {
    this.http.get(environment.hermexApiUrl+"/locations").subscribe((res:any)=>{
      this.tracked = res;
    })
  }
}
