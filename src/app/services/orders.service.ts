import { Injectable, OnInit } from '@angular/core';
import { Basket, Customer, Item, Order } from './orders.interfaces';
import { RidersService } from './riders.service';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { FetcherService } from './fetcher.service';
import { fetcher } from '../models/fetcher.model';

const lostItemsRate = 0.02
const riderOnBreakChance = 0.1
const breakTime = 1200

@Injectable({
  providedIn: 'root'
})
export class OrdersService{

  public fetcher : fetcher | undefined

  public statesDescription:any = {
    "queued" : { name: 'En cola' , color: 'yellow', description: "El pedido esta siendo asignado a un rider"} ,
    "assigned": { name: 'Asignado a rider' , color: 'lightblue', description: "El pedido ha sido asignado a un rider" },
    "accepted": { name: 'Aceptado por rider' , color: 'blue', description: "El pedido ha sido aceptado por un rider"},
    "near store": { name: 'Cercano a tienda' , color: 'blue', description: "El rider esta cercano a la Lomi store"},
    "picked": { name: 'En camino a destino' , color: 'blue', description: "El rider esta llevando el pedido a su destino"},
    "near dropoff": { name: 'Cercano a destino' , color: 'blue', description: "El rider esta cercano al lugar de destino"},
    "dropoff": { name: 'Entregado' , color: 'green', description: "El rider ha entregado el pedido"}
  }

  public orders:Array<any> = []
  public currentPage = 0
  public pageSize = 25
  public totalOrders = 0;
  public averageTime:number = 0;

  public flota:Array<Customer> = [
  ]

  public async getOrderByRefId(refId:string){
    console.log(refId)
    let order = await this.http.get("https://lomi.cl/api/orders/"+refId+"?token="+environment.spreeToken).toPromise()
    return order
  }

  public nextPage(){
    this.currentPage = this.currentPage - 1;
    this.getOrders();
  }

  public prevPage(){
    this.currentPage++;
    this.getOrders();
  }
  
  public async setPage(pageNumber: number){
    this.currentPage = pageNumber
    console.log(await this.fetchOrders(), this.currentPage)
    return await this.fetchOrders();
  }

  async fetchOrders(){

    let res:any =  await this.http.get(environment.hermexApiUrl + "/api/orders?"+"limit="+this.pageSize+"&page=" + this.currentPage).toPromise();
    this.averageTime = res.data.rows.reduce((total:any, value:any)=>{
      if(this.minutesFromQueued(value) > 60) return total;
      return typeof total == typeof 0? total + this.minutesFromQueued(value) : this.minutesFromQueued(value);
    })/ res.data.rows.length
    return res
  }


  public newOrder = (orderInfo:any) => {
    this.http.post(environment.hermexApiUrl+"/placeOrder", orderInfo).subscribe((res)=>{
      console.log(res)
    })
  }

  private getOrders = () => {
    console.log("getting orders");
    this.http.get(environment.hermexApiUrl+"/api/orders").subscribe((orders:any)=>{
      this.orders = orders.data.rows.sort((a:any,b:any)=> a.id - b.id)
      console.log(this.orders)
      this.totalOrders = orders
    })
  }

  async getOrderById(orderId:any){
    let order:any = await this.http.get(environment.hermexApiUrl+"/orders/"+orderId).toPromise()
    console.log("ORDER", order.data)
    order.data.states.sort((state1:any,state2:any)=> 
    {
      console.log(state1);
      return new Date(state1.createdAt).getTime() - new Date(state2.createdAt).getTime();
    })
    return order.data
  } 

  constructor(private http:HttpClient, private ridersService:RidersService, private fetcherService:FetcherService) {    
    this.fetchData()
  }
  
  async fetchData(){
    this.fetcher = await this.fetcherService.fetchObject({
      model: "orders",
      include: JSON.stringify(["rider","status","store", "customer"])
    })
    console.log("subscribing")
    this.fetcher.currentData?.subscribe((data)=>{
      console.log("Update service orders",data?.rows)
      this.orders = data?.rows;
    })
  }

  public get averageDeliveryTime(){
    if(!this.orders) return;
    let totalTime = 0;
    this.orders.forEach(order=>{
      totalTime += order.estimatedTime? order.estimatedTime : 0
    })
    return ((totalTime / this.orders.length)/60).toFixed()
  } 

  public get maxTime(){
    if(!this.orders) return;
    let maxTime = 0;
    this.orders.forEach(order=>{
      if(order.estimatedTime > maxTime){
        maxTime = order.estimatedTime
      }
    })
    return (maxTime/60).toFixed()
  } 

  public async cancelOrder(order:any){
    let res:any =  await this.http.post(environment.hermexApiUrl +"/order/"+order.id+ "/changeStatus", {
      statusId: 9
    }).toPromise();
  }

  public completeOrder(order:any){
    let res:any =  this.http.post(environment.hermexApiUrl +"/order/"+order.id+ "/changeStatus", {
      statusId: 8
    }).toPromise();
  }

  public get averageCost(){
    if(!this.orders) return;
    let totalCost = 0;
    this.orders.forEach(order=>{
      if(order.estimatedCost){
        totalCost += order.estimatedCost
      }
    })
    return ((totalCost / this.orders.length)*2).toFixed()
  }

  public notFinished(){
    return this.orders.filter((order)=>{
      order.currentStatusId != 8;
    })
  }

  public inQueue(){
    if(!this.orders) return;
    return this.orders.filter((order)=>{
      if(order.states?.length && order.states.length <= 1){
        return true
      } else {
        return false
      }
    })
  }

  
  public toPickup(){
    return this.orders.filter((order)=>{
      if(order.states?.length && order.states.length > 1 && order.states.length <= 4){
        return true
      } else {
        return false
      }
    })
  }

  public inDelivery(){
    return this.orders.filter((order)=>{
      if(order.states?.length && order.states.length > 4 && order.states.length <= 6){
        return true
      } else {
        return false
      }
    })
  }

  public basketValue(items:any){
    let sum = 0
    items.forEach((item:any) => {
      sum += item.price * item.quantity;
    });
    return sum.toFixed();
  }
  
  public async changeState(orderId:number, statusId:number, riderId:number){
    let res:any =  await this.http.post(environment.hermexApiUrl +"/order/"+orderId+ "/changeStatus", {
      statusId,
      riderId
    }).toPromise();
    console.log(res)
  }

  public outOfTime(){
    return this.orders.filter((order)=>{
      if(order.states?.length && order.states.length <= 5){
        console.log(order.states.length)
        console.log(order.states.length)
      }
      return true
    })
  }

  public minutesFromQueued(order:any):number{
<<<<<<< HEAD
    if(!order.states){
      return 0
    }
=======
>>>>>>> old-console
    let date = order.states.find((state:any)=> state.statusId >= 8)?.createdAt;    
    if(date){
      return parseInt(((new Date(date).getTime()  - new Date(order.createdAt).getTime())/ (1000 * 60)).toFixed())
    } else {
      return parseInt(((new Date().getTime() - new Date(order.createdAt).getTime())/ (1000 * 60)).toFixed())
    }
  }

  public isFinished(order:any){
    let date = order.states.find((state:any)=> state.statusId == 8)?.createdAt;
    return date ? true : false
  }

  public getEndDate(order:any){
    let date = order.states.find((state:any)=> state.statusId == 8)?.createdAt;
    return date? new Date(date) : null;
  }
}
