export interface Customer{
    name: string
    state: string
}

export interface Item{
    name: string
    price: number
    quantity?: number
}

export interface Basket{
    items: Array<Item>
}

export interface Location{
    lat: number,
    lng: number
}

export interface Order{
    //From integrations
    customer: Customer
    location: Location
    basket: Basket

    //Calculated in real time
    rider: any
    finishDate?: Date
    lastState?: String
    states?: Array<Array<String>>
    polyline?: Array<any>
    estimatedTime: number
    estimatedCost: number
}